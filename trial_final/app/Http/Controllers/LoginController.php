<?php

namespace App\Http\Controllers;

use App\Models\Nasabah;
use App\Models\Pinjaman;
use App\Models\Simpanan;
use App\Models\Status;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index(){
        return view('login');
    }
    public function login(Request $request){
        $request->validate([
            'email'=>'required|email',
            'password'=>'required'
        ]);
        $credentials=$request->only('email','password');

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return redirect('/nasabah');
        }

        return back()->withErrors([
            'error' => 'Email/Password Tidak Terdaftar.',
        ])->withInput();
    }
    public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/');
    }
    public function dashboard()
    { 
        $nasabah=Nasabah::count();
        $nasabaht=Nasabah::paginate(5);
        $pinjaman=Pinjaman::count();
        $simpanan=Simpanan::count();
        $simpanant=Simpanan::paginate(5);
        $user=User::count();
        return view('dashboard.index', compact('nasabah','nasabaht','pinjaman', 'simpanan','simpanant', 'user'));
    }
}

