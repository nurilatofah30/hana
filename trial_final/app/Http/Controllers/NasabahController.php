<?php

namespace App\Http\Controllers;

use App\Models\Nasabah;
use Illuminate\Http\Request;

class NasabahController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $nasabah=Nasabah::all();
        return view('nasabah.index', compact('nasabah'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
       return view ('nasabah.create'); 
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama'=> 'required',
            'tempat_lahir'=> 'required',
            'tanggal_lahir'=> 'required',
            'no_nik'=> 'required',
            'no_telp'=> 'required',
            'email'=> 'required',
            'pendidikan'=> 'required',
            'profesi'=> 'required',
            'sumber_penghasilan'=> 'required',
            'provinsi'=> 'required',
            'alamat'=> 'required',
            'nama_ibu'=> 'required',
            'alamat_ibu'=> 'required',
            'no_nik_ibu'=> 'required',
            'nama_ayah'=> 'required',
            'alamat_ayah'=> 'required',
            'no_nik_ayah'=> 'required',
         ]);
         $data=$request->all();
         Nasabah::create($data);
         return redirect('/nasabah')->with('sukses','Data Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     */
    public function show(Nasabah $nasabah)
    {
        return view('nasabah.detail', compact('nasabah'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Nasabah $nasabah)
    {
        return view('nasabah.edit', compact('nasabah'));
        
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Nasabah $nasabah)
    {
        $request->validate([
            'nama'=> 'required',
            'tempat_lahir'=> 'required',
            'tanggal_lahir'=> 'required',
            'no_nik'=> 'required',
            'no_telp'=> 'required',
            'email'=> 'required',
            'pendidikan'=> 'required',
            'profesi'=> 'required',
            'sumber_penghasilan'=> 'required',
            'provinsi'=> 'required',
            'alamat'=> 'required',
            'nama_ibu'=> 'required',
            'alamat_ibu'=> 'required',
            'no_nik_ibu'=> 'required',
            'nama_ayah'=> 'required',
            'alamat_ayah'=> 'required',
            'no_nik_ayah'=> 'required',
         ]);

         $dl=Nasabah::findOrfail($nasabah->id);
         $db=$request->all();
         $dl->update($db);
         return redirect('/nasabah')->with('sukses','Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Nasabah $nasabah)
    {
        Nasabah::destroy($nasabah->id);
        return redirect('/nasabah')->with('sukses','Data Berhasil Dihapus');
    }
}
