<?php

namespace App\Http\Controllers;

use App\Models\Pinjaman;
use App\Models\Nasabah;
use Illuminate\Http\Request;

class PinjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pinjaman=Pinjaman::all();
        return view('pinjaman.index',compact('pinjaman'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('pinjaman.create',[
            'nasabahs'=>Nasabah::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_id' => 'required',
            'jumlah_pinjaman' => 'required',
            'jangka_waktu_peminjaman' => 'required',
            'tanggal_pencairan' => 'required',
        ]);

        $data=$request->all();
        Pinjaman::create($data);
        return redirect('/pinjaman');
    }

    /**
     * Display the specified resource.
     */
    public function show(Pinjaman $pinjaman)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Pinjaman $pinjaman)
    {
        // $pinjaman=Pinjaman::all();
        // $nasabah=Nasabah::all();
        
        // return view ('pinjaman.edit',compact('nasabah','pinjaman'));

        return view('pinjaman.create',[
            'nasabahs'=>Nasabah::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Pinjaman $pinjaman)
    {
        $request->validate([
            'nama_id' => 'required',
            'jumlah_pinjaman' => 'required',
            'jangka_waktu_peminjaman' => 'required',
            'tanggal_pencairan' => 'required',
        ]);

        $dl=Pinjaman::findOrfail($pinjaman->id);
         $db=$request->all();
         $dl->update($db);
         return redirect('/pinjaman')->with('sukses','Data Peminjaman Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Pinjaman $pinjaman)
    {
        Pinjaman::destroy($pinjaman->id);
        return redirect('/pinjaman')->with('sukses','Data Berhasil Dihapus');
    }
}
