<?php

namespace App\Http\Controllers;

use App\Models\Simpanan;
use App\Models\Nasabah;
use Illuminate\Http\Request;

class SimpananController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $simpanan=Simpanan::all();
        return view('simpanan.index',compact('simpanan'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('simpanan.create',[
            'nasabahs'=>Nasabah::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_id' => 'required',
            'jumlah_setoran' => 'required',
            'tanggal_setoran' => 'required',
        ]);

        $data=$request->all();
        Simpanan::create($data);
        return redirect('/simpanan')->with('sukses','Data Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     */
    public function show(Simpanan $simpanan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Simpanan $simpanan)
    {
        return view('simpanan.create',[
            'nasabahs'=>Nasabah::all()
        ]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Simpanan $simpanan)
    {
        $request->validate([
            'nama_id' => 'required',
            'jumlah_setoran' => 'required',
            'tanggal_setoran' => 'required',
        ]);

         $dl=Simpanan::findOrfail($simpanan->id);
         $db=$request->all();
         $dl->update($db);
         return redirect('/simpanan')->with('sukses','Data Peminjaman Berhasil Diubah');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Simpanan $simpanan)
    {
        Simpanan::destroy($simpanan->id);
        return redirect('/simpanan')->with('sukses','Data Berhasil Dihapus');
    }
}
