<?php

namespace App\Http\Controllers;

use App\Models\Status;
use App\Models\Nasabah;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $status=Status::all();
        return view('status.index',compact('status'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('status.create',[
            'nasabahs'=>Nasabah::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_id' => 'required',
            'status' => 'required',
        ]);

        $data=$request->all();
        Status::create($data);
        return redirect('/status')->with('sukses','Data Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     */
    public function show(Status $status)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Status $status)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Status $status)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Status $status)
    {
        Status::destroy($status->id);
        return redirect('/status')->with('sukses','Data Berhasil Dihapus');
    }
}
