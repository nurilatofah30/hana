<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('nasabahs', function (Blueprint $table) {
            $table->id();
            $table->string('nama', 255);
            $table->string('tempat_lahir', 255);
            $table->date('tanggal_lahir');
            $table->string('no_nik', 16);
            $table->string('no_telp', 13);
            $table->string('email', 155);
            $table->enum('pendidikan', ['S3','S2','S1','Diploma','SMK/SMA/Sederajat', 'SMP/MTs/Sederajat','SD','TK','Lainnya']);
            $table->string('profesi', 155);
            $table->string('sumber_penghasilan', 155);
            $table->string('provinsi', 155);
            $table->string('alamat', 255);
            $table->string('nama_ayah', 255);
            $table->string('no_nik_ayah', 255);
            $table->string('alamat_ayah', 255);
            $table->string('nama_ibu', 255);
            $table->string('no_nik_ibu', 255);
            $table->string('alamat_ibu', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('nasabahs');
    }
};
