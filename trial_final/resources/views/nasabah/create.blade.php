@extends('template.main')
@section('judul','Form Tambah Data Nasabah')
@section('nasabah','active')
@section('konten')
<form action="/nasabah" method="POST">
    @csrf
    <!-- Awal Data Diri -->
    <div class="card shadow pe-4 pt-4 px-4 pb-3">
        <div class="mb-3">
            <h4>Masukkan Data Diri Anda</h4>
        </div>
        <div class="mb-3">
            <label class="title-section-content" for="">Nama Lengkap</label>
            <input value="{{old('nama')}}" name="nama" type="text"
                class="form-control @error('nama') is-invalid @enderror" placeholder="Masukkan Nama Lengkap Anda">
            @error('nama')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
        <div class="mb-3">
            <label class="title-section-content" for="">Tempat Lahir</label>
            <input value="{{old('tempat_lahir')}}" name="tempat_lahir" type="text"
                class="form-control @error('tempat_lahir') is-invalid @enderror" placeholder="Masukkan Tempat Lahir Anda">
            @error('tempat_lahir')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
        
        <div class="mb-3">
            <label class="title-section-content" for="">Tanggal Lahir</label>
            <input value="{{old('tanggal_lahir')}}" name="tanggal_lahir" type="date"
                class="form-control @error('tanggal_lahir') is-invalid @enderror">
            @error('tanggal_lahir')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
        <div class="mb-3">
            <label class="title-section-content" for="">Nomor Nik</label>
            <input value="{{old('no_nik')}}" name="no_nik" type="text"
                class="form-control @error('no_nik') is-invalid @enderror" placeholder="Masukkan No NIK Anda">
            @error('no_nik')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
        <div class="mb-3">
            <label class="title-section-content" for="">Nomor Telepon</label>
            <input value="{{old('no_telp')}}" name="no_telp" type="text"
                class="form-control @error('no_telp') is-invalid @enderror" placeholder="Masukkan Nomor Telepon Anda">
            @error('no_telp')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
        <div class="mb-3">
            <label class="title-section-content" for="">Email</label>
            <input value="{{old('email')}}" name="email" type="text"
                class="form-control @error('email') is-invalid @enderror" placeholder="Masukkan Alamat Email Anda">
            @error('email')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
        <div class="mb-3">
            <label class="title-section-content" for="">Pendidikan</label>
            <select name="pendidikan" id="" class="form-control @error('pendidikan') is-invalid @enderror">
                <option value="">--Pilih Tingkatan--</option>
                <option value="S3">S3</option>
                <option value="S2">S2</option>
                <option value="S1">S1</option>
                <option value="Diploma">Diploma</option>
                <option value="SMK/SMA/Sederajat">SMK/SMA/Sederajat</option>
                <option value="SMP/MTs/Sederajat">SMP/MTs/Sederajat</option>
                <option value="SD">SD</option>
                <option value="TK">TK</option>
            </select>
            @error('profesi')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
        <div class="mb-3">
            <label class="title-section-content" for="">Profesi</label>
            <input value="{{old('profesi')}}" name="profesi" type="text"
                class="form-control @error('profesi') is-invalid @enderror" placeholder="Masukkan profesi Anda">
            @error('profesi')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
        
        <div class="mb-3">
            <label class="title-section-content" for="">Sumber Penghasilan</label>
            <input value="{{old('sumber_penghasilan')}}" name="sumber_penghasilan" type="text"
                class="form-control @error('sumber_penghasilan') is-invalid @enderror mb-1" placeholder="Masukkan Sumber Penghasilan Anda">
                <span class="text-secondary">[Gaji, Bisnis, Investasi, Komisi, Orang Lain, Simpanan Pribadi, Lainnya]</span>
            @error('sumber_penghasilan')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
    </div>
    <!-- Akhir Data Diri -->

    <div class="mb-5"></div>

    <!-- Awal Alamat -->
    <div class="card shadow pe-4 pt-4 px-4 pb-3">
        <div class="mb-3">
            <h4>Masukkan Alamat Diri Anda</h4>
        </div>
        <div class="mb-3">
            <label class="title-section-content" for="">Wilayah/Domisili</label>
            <input value="{{old('provinsi')}}" name="provinsi" type="text"
                class="form-control @error('provinsi') is-invalid @enderror" placeholder="Masukkan Domisili Anda">
            @error('provinsi')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
               <div class="mb-3">
            <label class="title-section-content" for="">Alamat</label>
            <input value="{{old('alamat')}}" name="alamat" type="text"
                class="form-control @error('alamat') is-invalid @enderror" placeholder="Masukkan Alamat Anda">
            @error('alamat')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
    </div>
    <!-- Akhir Alamat -->

    <div class="mb-5"></div>

    <!-- Awal biodata ortu -->
    <div class="card shadow pe-4 pt-4 px-4 pb-3">
    <div class="mb-3">
            <h4>Masukkan Data Orang Tua Anda</h4>
        </div>
 
        
        <div class="mb-1">
            <h5>Ibu Kandung</h5>
        </div>
        <div class="mb-3">
            <label class="title-section-content" for="">Nama Ibu</label>
            <input value="{{old('nama_ibu')}}" name="nama_ibu" type="text"
                class="form-control @error('nama_ibu') is-invalid @enderror" placeholder="Masukkan Nama Ibu Anda">
            @error('nama_ibu')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
        <div class="mb-3">
            <label class="title-section-content" for="">Alamat Ibu</label>
            <input value="{{old('alamat_ibu')}}" name="alamat_ibu" type="text"
                class="form-control @error('alamat_ibu') is-invalid @enderror" placeholder="Masukkan Alamat Ibu Anda">
            @error('alamat_ibu')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
        <div class="mb-3">
            <label class="title-section-content" for="">No NIK Ibu</label>
            <input value="{{old('no_nik_ibu')}}" name="no_nik_ibu" type="text"
                class="form-control @error('no_nik_ibu') is-invalid @enderror mb-3" placeholder="Masukkan No NIK Ibu nda">
            @error('no_nik_ibu')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
        <div class="mb-1">
            <h5>Ayah Kandung</h5>
        </div>
        <div class="mb-3">
            <label class="title-section-content" for="">Nama Ayah</label>
            <input value="{{old('nama_ayah')}}" name="nama_ayah" type="text"
                class="form-control @error('nama_ayah') is-invalid @enderror" placeholder="Masukkan Nama ayah Anda">
            @error('nama_ayah')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
        <div class="mb-3">
            <label class="title-section-content" for="">Alamat Ayah</label>
            <input value="{{old('alamat_ayah')}}" name="alamat_ayah" type="text"
                class="form-control @error('alamat_ayah') is-invalid @enderror" placeholder="Masukkan Alamat ayah Anda">
            @error('alamat_ayah')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
        <div class="mb-3">
            <label class="title-section-content" for="">No NIK Ayah</label>
            <input value="{{old('no_nik_ayah')}}" name="no_nik_ayah" type="text"
                class="form-control @error('no_nik_ayah') is-invalid @enderror" placeholder="Masukkan No NIK ayah nda">
            @error('no_nik_ayah')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
    </div>
    
    <!-- Akhir biodata ortu -->
    
    <div class="mb-3 mt-4">
        <a href="/nasabah" class="btn btn-secondary">Kembali</a>
        <button class="btn btn-primary" type="submit">Tambah Data</button>
    </div>
</form>
@endsection