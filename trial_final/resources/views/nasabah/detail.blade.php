@extends('template.main')
@section('judul','Detail Data Nasabah')
@section('nasabah','active')
@section('konten')

<h3 class="text-center mb-4">Data Diri Nasabah</h3>
<form action="">
    <div class="row">
        <div class="col-5 offset-1">
            <div class="mb-3">
                <div class="card" style="width: 400px; height: 250px; ">
                <img src="1.jpg" alt="">
                </div>
            </div>
        </div>
        <div class="col-5">
            <div class="mb-3">
                <b> <label for="">Nama</label> </b><br>
                <label for="" class="form-control card">{{$nasabah->nama}}</label>
            </div>

            <div class="mb-3">
                <b> <label for="">Tempat Lahir</label> </b><br>
                <label for="" class="form-control card">{{$nasabah->tempat_lahir}}</label>
            </div>

            <div class="mb-3">
                <b> <label for="">Tanggal Lahir</label> </b><br>
                <label for="" class="form-control card">{{$nasabah->tanggal_lahir}}</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-5 offset-1">
        <div class="mb-3">
                <b> <label for="">No NIK</label> </b><br>
                <label for="" class="form-control card ">{{$nasabah->no_nik}}</label>
            </div>

            <div class="mb-3">
                <b> <label for="">No Telp</label> </b><br>
                <label for="" class="form-control card">{{$nasabah->no_telp	}}</label>
            </div>

            <div class="mb-3">
                <b> <label for="">Email</label> </b><br>
                <label for="" class="form-control card">{{$nasabah->email}}</label>
            </div>
        </div>
        <div class="col-5">
            <div class="mb-3">
                <b> <label for="">Pendidikan</label> </b><br>
                <label for="" class="form-control card">{{$nasabah->pendidikan}}</label>
            </div>

            <div class="mb-3">
                <b> <label for="">Profesi</label> </b><br>
                <label for="" class="form-control card">{{$nasabah->profesi}}</label>
            </div>

            <div class="mb-3">
                <b> <label for="">Sumber Penghasilan</label> </b><br>
                <label for="" class="form-control card">{{$nasabah->sumber_penghasilan}}</label>
            </div>
        </div>
    </div>


    
</form>

<h3 class="text-center mt-4 mb-3">Alamat Diri Nasabah</h3>
<form action="">
    <div class="mb-3 col-10 offset-1">
    <b> <label for="">Provinsi</label> </b><br>
    <label for="" class="form-control card">{{$nasabah->provinsi}}</label>
    </div>
    <div class="mb-3 col-10 offset-1">
    <b> <label for="">Alamat</label> </b><br>
    <label for="" class="form-control card">{{$nasabah->alamat}}</label>
    </div>
</form>

<h3 class="text-center mt-4 mb-3">Data Diri Orang Tua Nasabah</h3>
<form action="">
    <div class="row">
        <div class="col-5 offset-1 mt-2">
            <h4>Ibu</h4>
            <div class="mb-3">                
            <b> <label for="">nama_ibu</label> </b><br>
                <label for="" class="form-control card">{{$nasabah->nama_ibu}}</label>
            </div>
            <div class="mb-3">                
            <b> <label for="">No NIK Ibu</label> </b><br>
                <label for="" class="form-control card">{{$nasabah->no_nik_ibu}}</label>
            </div>
            <div class="mb-3">                
            <b> <label for="">Alamat Ibu</label> </b><br>
                <label for="" class="form-control card">{{$nasabah->alamat_ibu}}</label>
            </div>
        </div>
        <div class="col-5 ">
            <h4>Ayah</h4>
            <div class="mb-3 mt-2">                
            <b> <label for="">Nama Ayah</label> </b><br>
                <label for="" class="form-control card">{{$nasabah->nama_ayah}}</label>
            </div>
            <div class="mb-3">                
            <b> <label for="">No NIK Ayah</label> </b><br>
                <label for="" class="form-control card">{{$nasabah->no_nik_ayah}}</label>
            </div>
            <div class="mb-3">                
            <b> <label for="">Alamat Ayah</label> </b><br>
                <label for="" class="form-control card">{{$nasabah->alamat_ayah}}</label>
            </div>
        </div>
    </div>
</form>



<div class="mb-4 mt-4 text-center">
        <a href="/nasabah" class="btn btn-secondary">Kembali</a>
</div>


@endsection
