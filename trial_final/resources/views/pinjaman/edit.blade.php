@extends('template.main')
@section('judul','Form Edit Data peminjam')
@section('pinjaman','active')
@section('konten')
<form action="/pinjaman/{{$pinjaman->id}}" method="POST" class="col-8 offset-2">
    @csrf
    @method('put')
    <div class="mb-3">
        <label for="exampleFormControlInput1" class="title-section-content">Nama</label>
        <select class="form-select" name="nama_id" aria-label="Default select example">
            <option value="">-- Pilih Nasabah --</option>
            @foreach ($nasabahs as $k)
            @if (old('nama_id')==$k->id)
            <option value="{{ $k->id }}" selected>{{$k->nama}}</option>
            @else
            <option value="{{ $k->id }}">{{$k->nama}}</option>
            @endif
            @endforeach
        </select>
    </div>
    <div class="mb-3">
    <label class="title-section-content" for="">Jumlah Pinjaman</label>
    <input value="{{$pinjaman->jumlah_nn}}" name="jumlah_pinjaman" type="text"
        class="form-control @error('jumlah_pinjaman') is-invalid @enderror"
        placeholder="Masukkan Tanggal Pencairan Anda">
    @error('jumlah_pinjaman')
    <div class="invalid-feedback"> {{$message}} </div>
    @enderror
    </div>
    <div class="mb-3">
        <label class="title-section-content" for="">Jangka Waktu Peminjaman</label>
        <input value="{{$pinjaman->jangka_waktu_peminjaman}}" name="jangka_waktu_peminjaman" type="text"
            class="form-control @error('jangka_waktu_peminjaman') is-invalid @enderror"
            placeholder="Masukkan Jangka Waktu Anda">
        @error('jangka_waktu_peminjaman')
        <div class="invalid-feedback"> {{$message}} </div>
        @enderror
    </div>
    <div class="mb-3">
        <label class="title-section-content" for="">Tanggal Pencairan</label>
        <input value="{{$pinjaman->tanggal_pencairan}}" name="tanggal_pencairan" type="date"
            class="form-control @error('tanggal_pencairan') is-invalid @enderror"
            placeholder="Masukkan Jangka Waktu Anda">
        @error('tanggal_pencairan')
        <div class="invalid-feedback"> {{$message}} </div>
        @enderror
    </div>

    <div class="mb-3">
        <label class="title-section-content" for="">Tenggat Pembayaran</label>
        <input value="{{$pinjaman->tenggat_pembayaran')}}" name="tenggat_pembayaran" type="date"
            class="form-control @error('tenggat_pembayaran') is-invalid @enderror"
            placeholder="Masukkan Jangka Waktu Anda">
        @error('tenggat_pembayaran')
        <div class="invalid-feedback"> {{$message}} </div>
        @enderror
    </div>

    <div class="mb-3 mt-4">
        <a href="/pinjaman" class="btn btn-secondary">Kembali</a>
        <button class="btn btn-primary" type="submit">Edit Data</button>
    </div>
</form>
@endsection
