@extends('template.main')
@section('judul','Form Tambah Data Simpanan')
@section('simpanan','active')
@section('konten')
<form action="/simpanan" method="POST" class="col-8 offset-2">
    @csrf
    <div class="mb-3">
        <label for="" class="title-section-content">Nama</label>
        <select class="form-select" name="nama_id" aria-label="Default select example">
            <option value="">-- Pilih Nasabah --</option>
            @foreach ($nasabahs as $k)
            @if (old('nama_id')==$k->id)
            <option value="{{ $k->id }}" selected>{{$k->nama}}</option>
            @else
            <option value="{{ $k->id }}">{{$k->nama}}</option>
            @endif
            @endforeach
        </select>
    </div>
    <div class="mb-3">
    <label class="title-section-content" for="">Jumlah Setoran</label>
    <input value="{{old('jumlah_setoran')}}" name="jumlah_setoran" type="text"
        class="form-control @error('jumlah_setoran') is-invalid @enderror"
        placeholder="Masukkan Tanggal Pencairan Anda">
    @error('jumlah_setoran')
    <div class="invalid-feedback"> {{$message}} </div>
    @enderror
    </div>
    <div class="mb-3">
        <label class="title-section-content" for="">Tanggal Setoran</label>
        <input value="{{old('tanggal_setoran')}}" name="tanggal_setoran" type="date"
            class="form-control @error('tanggal_setoran') is-invalid @enderror"
            placeholder="Masukkan Jangka Waktu Anda">
        @error('tanggal_setoran')
        <div class="invalid-feedback"> {{$message}} </div>
        @enderror
    </div>

    <div class="mb-3 mt-4">
        <a href="/simpanan" class="btn btn-secondary">Kembali</a>
        <button class="btn btn-primary" type="submit">Tambah Data</button>
    </div>
</form>
@endsection
