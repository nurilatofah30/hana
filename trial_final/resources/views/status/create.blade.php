@extends('template.main')
@section('judul','Form Tambah Data Status')
@section('status','active')
@section('konten')
<form action="/status" method="POST" class="col-8 offset-2">
    @csrf
    <div class="mb-3">
        <label for="" class="title-section-content">Nama</label>
        <select class="form-select" name="nama_id" aria-label="Default select example">
            <option value="">-- Pilih Nasabah --</option>
            @foreach ($nasabahs as $k)
            @if (old('nama_id')==$k->id)
            <option value="{{ $k->id }}" selected>{{$k->nama}}</option>
            @else
            <option value="{{ $k->id }}">{{$k->nama}}</option>
            @endif
            @endforeach
        </select>
    </div>
    <div class="mb-3">
    <label class="title-section-content" for="">Status</label>
    <select name="status" id="" class="form-control @error('status') is-invalid @enderror">
        <option value="">--Pilih Status--</option>
        <option value="lunas">Lunas</option>
        <option value="belum_lunas">Belum Lunas</option>
    </select>
    @error('status')
    <div class="invalid-feedback"> {{$message}} </div>
    @enderror
    </div>


    <div class="mb-3 mt-4">
        <a href="/status" class="btn btn-secondary">Kembali</a>
        <button class="btn btn-primary" type="submit">Tambah Data</button>
    </div>
</form>
@endsection
