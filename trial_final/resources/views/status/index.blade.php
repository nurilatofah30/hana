@extends('template.main')
@section('judul','Data Status')
@section('status','active')
@section('konten')
<div class="product-card shadow">
    <a class="btn btn-primary mb-3 mt-3" href="status/create">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-square"
            viewBox="0 0 16 16">
            <path
                d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z" />
            <path
                d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
        </svg>
        Tambah Data
    </a>
    <table class="table">
        <thead>
            <th>No</th>
            <th>Nama</th>
            <th>Status</th>
            <th class="text-center">Aksi</th>
        </thead>
        <tbody>
            @foreach ($status as $n)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$n->nasabah->nama}}</td>
                <td>{{$n->status}}</td>
                <td class="text-center">
                    <form onclick="return confirm('Yakin Mau Dihapus?');" class="d-inline" action="/status/{{$n->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <button class="btn badge bg-danger" type="sumbit">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-trash-fill" viewBox="0 0 16 16">
                            <path
                                d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z" />
                            </svg>
                            Hapus
                        </button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@if (session('sukses'))
<script>
    Swal.fire({
        position: 'center',
        icon: 'success',
        title: "{{session('sukses')}}",
        showConfirmButton: false,
        timer: 1700
    })

</script>
@endif
@endsection