@extends('template.main')
@section('judul','Form Edit Data Pengguna')
@section('user','active')
@section('konten')
<form action="/user/{{$user->id}}" method="post">
    @csrf
    @method('put')
    <div class="mb-3">
        <label class="title-section-content" for="">Nama</label>
        <input value="{{$user->name}}" name="name" type="text"
            class="form-control @error('name') is-invalid @enderror">
        @error('name')
        <div class="invalid-feedback">
            {{$message}}
        </div>
        @enderror
    </div>
    <div class="mb-3">
        <label class="title-section-content" for="">Email</label>
        <input value="{{$user->email}}" name="email" type="email"
            class="form-control @error('email') is-invalid @enderror">
        @error('email')
        <div class="invalid-feedback">
            {{$message}}
        </div>
        @enderror
    </div>
    <div class="mb-3">
        <label class="title-section-content" for="">Phone</label>
        <input value="{{$user->phone}}" name="phone" type="text"
            class="form-control @error('phone') is-invalid @enderror">
        @error('phone')
        <div class="invalid-feedback">
            {{$message}}
        </div>
        @enderror
    </div>
    <div class="mb-3">
        <label class="title-section-content" for="">Role</label>
        <select name="role" id="" class="form-control @error('role') is-invalid @enderror">
            <option value="{{$user->role}}">--Pilih JIka Ingin Diubah</option>
            <option value="Admin">Admin</option>
            <option value="User">User</option>
        </select>
        @error('tanggal_terima')
        <div class="invalid-feedback">
            {{$message}}
        </div>
        @enderror
    </div>
    <div class="mb-3">
        <label class="title-section-content" for="">Password</label>
        <input value="{{$user->password}}" name="password" type="password"
            class="form-control @error('password') is-invalid @enderror">
        @error('password')
        <div class="invalid-feedback">
            {{$message}}
        </div>
        @enderror
    </div>
    <div class="mb-3">
        <a href="/user" class="btn btn-secondary">Kembali</a>
        <button class="btn btn-primary" type="submit">Edit Data</button>
    </div>
</form>
@endsection
