<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SimpananController;
use App\Http\Controllers\PinjamanController;
use App\Http\Controllers\NasabahController;
use App\Http\Controllers\StatusController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LoginController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('template.main');
// });
Route::get('/', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/', [LoginController::class, 'login'])->middleware('guest');
Route::get('/logout', [LoginController::class, 'logout'])->middleware('auth');
Route::get('/home', [LoginController::class, 'dashboard'])->middleware('auth');



Route::resource('/nasabah', NasabahController::class)->middleware('auth');
Route::resource('/pinjaman', PinjamanController::class)->middleware('auth');
Route::resource('/simpanan', SimpananController::class)->middleware('auth');
Route::resource('/status', StatusController::class)->middleware('auth');
Route::resource('/user', UserController::class)->middleware('auth');
